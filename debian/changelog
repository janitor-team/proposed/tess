tess (0.3.0-10) unstable; urgency=medium

  * Adopt package (Closes: #748268)
  * d/control: Add Multi-Arch: foreign

 -- Rafael Laboissière <rafael@debian.org>  Thu, 02 Dec 2021 12:50:24 -0300

tess (0.3.0-9) unstable; urgency=medium

  * QA upload.

  * d/control: Drop versioned constraint on jed and slrn in Suggests
  * Proper running of build-time and autopkgtest unit tests
    + d/p/slsh-path-unit-tests.patch: New patch
    + d/rules: Use the environment variable SLSH_PATH
    + d/t/control: Depends on slsh
    + d/t/run-tests: New script

 -- Rafael Laboissière <rafael@debian.org>  Thu, 07 Oct 2021 09:18:36 -0300

tess (0.3.0-8) unstable; urgency=medium

  * QA upload.

  * Create Git repository at Salsa.debian.org.
    The Git repository was created with "gbp import-dscs --debsnap",
    instead of doing archeological work in the Alioth archives. The
    layout of the repository follows the recommendation in DEP-14.
  * d/s/format: Use source format 3.0 (quilt)
  * Bump debhelper compatibility level to 13
    + d/control: Set dependency on debhelper-compat = 13
    + d/compat: Drop file
  * d/control:
    + Bump Standards-Version to 4.6.0 (no changes needed)
    + Add Rules-Requires-Root: no
  * Switch from CDBS to debhelper
    + d/control: Drop the build-dependencies on cdbs and quilt
    + d/rules: Use the dh sequencer
  * d/copyright: Convert to DEP-5 format
  * Activate dh_autoreconf
    + d/rules: Move the generated autoconf/configure to the top directory
    + d/autoreconf: New file
    + d/p/update-configure.patch: New patch
    + d/source/options: Ignore the configure file
    + d/clean: Remove leftover directory admin/admin/
  * d/t/control: Add autopkgtest support
  * d/watch: Bump version to 4 (no changes)
  * d/s/lintian-overrides: Override "non secure URI" warning

 -- Rafael Laboissière <rafael@debian.org>  Mon, 27 Sep 2021 15:57:28 -0300

tess (0.3.0-7.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 14:58:26 +0100

tess (0.3.0-7) unstable; urgency=medium

  * QA (group) upload
  * d/control:
    + Remove myself from the Uploaders list
    + Set Maintainer to Debian QA Group (orhpaned package)

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 16 Jul 2016 16:52:25 -0300

tess (0.3.0-6) unstable; urgency=low

  * debian/control:
    + Bump Standards-Version to 3.8.1 (add debian/README.source file telling
      that quilt is used to obtain the upstream sources patched for Debian)
    + The package is now maintained with Git at alioth.debian.org.  Add
      Vcs-Git field and change Vcs-Browser accordingly.
    + Fix Lintian warning debhelper-but-no-misc-depends
    + Bump build dependency on debhelper to >= 7
  * debian/compat: Set compatibility level of debhelper to 7
  * debian/patches/dest-sl-files-install-dir.patch: Add description (fix
    Lintian warning)
  * debian/watch: Add file for uscan

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 26 Apr 2009 13:25:05 +0200

tess (0.3.0-5) unstable; urgency=low

  * Switch from CDBS' simple-patchsys to quilt
  * debian/control: Moved libslang2-dev and slsh into Build-Depends-Indep

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 17 Mar 2008 16:36:36 +0100

tess (0.3.0-4) unstable; urgency=low

  * debian/control: Changed architecture of slang-tess package to "all"
    instead of "any", since this is actually an architecture-independent
    package.  Thanks to Raphael Geissert and his script for spotting this.

 -- Rafael Laboissiere <rafael@debian.org>  Tue, 08 Jan 2008 09:00:21 +0100

tess (0.3.0-3) unstable; urgency=low

  * debian/control:
    + Added Vcs-Svn, Vcs-Browser, and Homepage fields
    + Dropped the Homepage pseudo-header from the extended description
    + Bumped Standards-Version to 3.7.3

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 15 Dec 2007 22:36:52 +0100

tess (0.3.0-2) unstable; urgency=low

  * debian/patches/dest-sl-files-install-dir.patch: Instead of using the
    hack in debian/rules introduced in the last version, use a patch to
    admin/Makefile.in.  That way, if/when the upstream author fixes the
    problem, I will notice that my changes must be reverted.

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 28 Mar 2007 02:02:25 +0200

tess (0.3.0-1) unstable; urgency=low

  * New upstream release.  The value of the DEB_MAKE_INSTALL_TARGET
    variable in debian/rules is adjusted, in order to fix a bug in the
    upstream admin/Makefile.in file.

  * debian/control:
    + Added space before pseudo-header Homepage, complying with the Debian
      Best Packaging Practices.
    + Added XS-Vcs-Svn and XS-Vcs-Browser

  * debian/rules: Dropped useless Depends fields

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 28 Mar 2007 00:57:42 +0200

tess (0.1.2.r3-3) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * debian/control:
    - Added slsh to build-depends, such that the package will build
      automatically from source (closes: #338931)
    - Removed obsolete build-dependencies on xsltproc and docbook-xsl

 -- Debian JED Group <pkg-jed-sl-modules@lists.alioth.debian.org>  Mon, 14 Nov 2005 11:14:02 +0100

tess (0.1.2.r3-1) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * New upstream release
  * debian/control: Changed the maintainer to
    pkg-jed-sl-modules@lists.alioth.debian.org
  * debian/rules: Dropped clean target, since the upstream "make
    distclean" does not let files behind
  * debian/watch: Added uscan file

 -- Debian JED Group <pkg-jed-sl-modules@lists.alioth.debian.org>  Tue,  8 Nov 2005 23:01:33 +0100

tess (0.1.2.r2-1) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * First official Debian release (closes: #337254)

 -- Debian JED Group <pkg-jed-devel@lists.alioth.debian.org>  Fri,  4 Nov 2005 11:48:51 +0100

tess (0.1.2-0.1) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * Initial release

 -- Debian JED Group <pkg-jed-devel@lists.alioth.debian.org>  Mon, 31 Oct 2005 18:56:51 +0100

Local Variables:
eval: (add-local-hook
  'debian-changelog-add-version-hook
  (lambda ()
    (save-excursion
      (forward-line -1)
      (beginning-of-line)
      (insert "\n   [ "
        (or (getenv "DEBFULLNAME") (user-full-name)) " ]"))))
End:
