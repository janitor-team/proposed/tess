tess

 DESCRIPTION
   TESS is the (Te)st (S)ystem for (S)-Lang, which aims at reducing the
   workload and ad-hoc nature of regression testing S-Lang software by
   by collecting common testing elements into a single, easy-to-use
   framework.  TESS provides the S-Lang developer nominal mechanisms
   for tailoring the S-Lang environment and invoking functions with
   arbitrary inputs, while transparently inspecting and cleaning the
   stack, gathering pass/fail statistics, and providing error recovery
   from exceptions.
   
   Copyright (C) 2004-2007 Massachusetts Institute of Technology 
   Michael S. Noble <mnoble@space.mit.edu>

 SEE ALSO
   tess_invoke, tess_summary, tess_add_eval_paths
--------------------------------------------------------------

tess_auto_summarize

 SYNOPSIS
   Turn automatic test suite summarization on or off

 USAGE
   tess_add_eval_paths( [0 | 1.])

 DESCRIPTION

 NOTES

 SEE ALSO
   tess_summary
--------------------------------------------------------------

tess_add_eval_paths

 SYNOPSIS
   Add one or more directories to the S-Lang evalfile() search path

 USAGE
   tess_add_eval_paths( path1, [path2, ...])

 DESCRIPTION
   This function is a convenience wrapper around the set_slang_load_path()
   function, making it cleaner and simpler to augment the list of directories
   searched by the S-Lang interpreter when evalfile() is invoked with an
   ambiguous file specification.

 NOTES
   TESS automatically appends the current working directory, as well as ../src,
   ../share, and ../packages to the load path.

 SEE ALSO
   tess_add_import_paths
--------------------------------------------------------------

tess_add_import_paths

 SYNOPSIS
   Add one or more directories to the S-Lang import() search path

 USAGE
   tess_add_import_paths( path1, [path2, ...])

 DESCRIPTION
   This function is a convenience wrapper around the set_import_module_path()
   function, making it cleaner and simpler to augment the list of directories
   searched by the S-Lang interpreter when import() is invoked.

 NOTES
   TESS automatically appends ../src to the import path.

 SEE ALSO
   tess_add_eval_paths
--------------------------------------------------------------

tess_catch_type_errors

 SYNOPSIS
   Give S-Lang ERROR block mechanism the ability to catch type mismatch errors

 USAGE
   tess_catch_type_errors( [yes_or_no] ) 

 DESCRIPTION
   This function augments the S-Lang ERROR block mechanism, giving it the
   ability to catch type mismatch exceptions (which S-Lang 1.x formally
   considers uncatchably fatal).  This feature is useful for a test
   framework, since it allows functions to be safely exercised against
   a wide variety of types.
   
   If the first passed argument evaluates to a boolean TRUE then the function
   will enable type error catching.  If either zero arguments are passed,
   or the first argument evaluates to boolean FALSE, then type error catching
   will be disabled.

 NOTES
   This function is deprecated, as in S-Lang 2 all exceptions may be caught.

 SEE ALSO
   tess_invoke
--------------------------------------------------------------

tess_invoke

 SYNOPSIS
   Execute a test case

 USAGE
   tess_invoke( expected_to_fail, function_ref [, arg1, arg2, ...]) 

 DESCRIPTION
   Invoke the given function (by dereference), optionally passing in one
   or more arguments.  The first parameter, whose value should be either
   zero or one, indicates whether the function is expected to signal an
   error when invoked in the manner given.
   
   If the actual result of the call matches the expected result then the test
   case is said to "pass," otherwise it is said to "fail".  It is important
   to understand this: a failed test case is not indicated by an error 
   signal itself, but rather by whether or not the test case expected an
   error to be signaled.

 NOTES

 SEE ALSO
   tess_catch_type_errors, tess_summary
--------------------------------------------------------------

tess_load_component

 SYNOPSIS
   Evaluate the named S-Lang script, and set the test component name accordingly

 USAGE
   tess_load_component(filename)

 DESCRIPTION
   This function attempts to evalfile() the named script, using the usual
   S-Lang load mechansism, and will set the TESS test component name to 
   the filename if found.
   
   The test component name is printed in the heading of results summaries,
   and uniquely identifies a given test script.  Typically the test component
   name is set to the "basename" of the test script itself (e.g. a script
   add.t sets Component = "add").  This function provides a means of 
   customizing that default behavior while loading additional functionality
   to be exercised within the test script.

 NOTES

 SEE ALSO
   
--------------------------------------------------------------

tess_summary

 SYNOPSIS
   Summarize the results of a suite of tests

 USAGE
   Integer_Type tess_summary()

 DESCRIPTION
   TESS automatically records the pass/fail result of each test case
   executed by tess_invoke.  By default the results of this tally
   are emitted to stdout when tess_summary is called, although this may
   be disabled by calling tess_auto_summarize(0).  The return value
   indicates the number of failed tests.

 NOTES
   Under normal circumstances it should not be necessary to call this
   function explicitly, since TESS transparently installs an exit handler
   which calls tess_summary at application termination.  Its return value
   is then passed to the operating so that, for example, a non-zero status
   may be used to fatally terminate a "make test" goal.
   

 SEE ALSO
   tess_invoke
--------------------------------------------------------------

