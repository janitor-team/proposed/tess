#i docbook_man.tm

#d arg#1 <arg>$1</arg>\__newline__

#d trun \command{tessrun}
#d jed \command{jed}
#d jedscript \command{jed-script}
#d slang \literal{S-Lang}
#d ifun#1 \literal{$1}
#d sfun#1 \literal{$1}

\manpage{\trun}{1}{convenience script for running TESS scripts en masse}

\mansynopsis{\trun}{
  \arg{\option{-h}}
  \arg{\option{-l}}
  \arg{\option{-l}}
  \arg{\option{application} \option{[ args ...]}}
}

\refsect1{DESCRIPTION}
  \p 
   The \trun script is part of TESS, the (Te)st (S)ystem for (S)-Lang.
   It is intended to simplify the invocation, typically within a Makefile,
   of TESS-based automated regression suites.  Each test in the current
   directory (marked by a .t suffix) will be automatically loaded into
   the S-Lang interpreter (within slsh, by default) and executed.
   \p
   \trun returns 1 if any tests fail, otherwise 0.
  \p-end
\refsect1-end

#d man_options_entry#2 \varlistentry{\term{$1}}{\p $2 \p-end}\__newline__

\refsect1{OPTIONS}
    \variablelist
      \man_options_entry{\option{-h}}{
        Output this short help text.
      }
      \man_options_entry{\option{-l}}{
	Supports local execution of examples (before install).
      }
      \man_options_entry{\option{-v}}{
	Verbose mode.
      }
      \man_options_entry{\option{application} \option{[ args ...]}}{
	Replace the use of slsh with the given application, and optional
	arguments.
      }
    \variablelist-end
\refsect1-end

\refsect1{AUTHOR}
  \p
  The author of TESS is Michael S. Noble <mnoble@space.mit.edu>.
  Rafael Laboissiere <rafael@debian.org> created the TESS package
  for Debian and helped author this manual page.
  \pp
  Permission is granted to copy, distribute and/or modify
  this document under the terms of the GNU General Public License,
  Version 2 any later version published by the Free Software
  Foundation.
  \pp
  On Debian systems, the complete text of the GNU General Public
  License can be found in \filename{/usr/share/common-licenses/GPL}
\p-end
\refsect1-end

\refsect1{SEE ALSO}
   \p
   On Debian systems the reference manual for TESS can be found at
   /usr/share/doc/slang-tess/tess.txt.gz.   It is also packaged in
   text and PDF forms in the source distribution.
\refsect1-end

\manpage-end
