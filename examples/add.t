
require("tess");

tess_invoke(1, &add);
tess_invoke(1, &add, "hi there!");
tess_invoke(1, &add, 2);
tess_invoke(0, &add, 2, 3);
	
tess_invoke(1, &add, "one", 2);
tess_invoke(0, &add, "hello", " there!");
